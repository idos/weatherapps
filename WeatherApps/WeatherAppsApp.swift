//
//  WeatherAppsApp.swift
//  WeatherApps
//
//  Created by R+IOS on 24/05/22.
//

import SwiftUI

@main
struct WeatherAppsApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
