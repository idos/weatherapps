//
//  ServiceAPI.swift
//  WeatherApps
//
//  Created by R+IOS on 24/05/22.
//

import Foundation

class ServiceAPI {
    func requestWeather(completion: @escaping (_ result: Result<ResponseWeather?, Error>) -> Void) {
        request { result in
            switch result {
            case .success(let data):
                guard let weather = try? JSONDecoder().decode(ResponseWeather.self, from: data) else {
                    completion(.failure("Data emptu" as! Error))
                    return
                }
                
                DispatchQueue.main.async {
                    completion(.success(weather))
                }
            case .failure(let error):
                print(error)
                completion(.failure(error))
            }
        }
    }
    
    private func request(method: Methods = .get, completion: @escaping (_ result: Result<Data, Error>) -> Void) {
        let session = URLSession.shared
        let url = URL(string: "https://api.openweathermap.org/data/2.5/onecall?lat=6.175392&lon=106.827153&exclude=hourly&appid=afcc1022dfa3ed2b9991fce1348d2d87")!
        let task = session.dataTask(with: url, completionHandler: { data, response, error in
            // Check the response
            print(response)
            
            // Check if an error occured
            if error != nil {
                // HERE you can manage the error
                print(error!)
                completion(.failure(error!))
                return
            }
            guard let data = data else {
//                completion(nil)
                return
            }
            completion(.success(data))
            
        })
        task.resume()
    }
}

enum Methods: String {
    case post = "POST"
    case get = "GET"
    case delete = "DELETE"
}
