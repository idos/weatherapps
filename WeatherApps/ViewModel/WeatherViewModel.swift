//
//  WheaterViewModel.swift
//  WeatherApps
//
//  Created by R+IOS on 25/05/22.
//

import Foundation
import Combine

class WeatherViewModel: ObservableObject {
    @Published var response: ResponseWeather?
    
    var service = ServiceAPI()
    var localData = LocalData()
    
    func getData() {
        
        service.requestWeather { result in
            switch result {
            case .success(let weather):
                self.response = weather
                self.localData.saveResponse(response: weather!)
                
            case .failure(let error):
                print(error)
                self.getDataFromLocal()
            }
        }
    }
    
    func getDataFromLocal() {
        let data = localData.getResponseLocal()
        
        self.response = data
    }
}
