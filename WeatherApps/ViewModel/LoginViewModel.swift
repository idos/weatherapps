//
//  LoginViewModel.swift
//  WeatherApps
//
//  Created by R+IOS on 24/05/22.
//

import Foundation
import Combine

class LoginViewModel: ObservableObject {
    @Published var email = ""
    @Published private(set) var validationMessageUsername: String?
    @Published var password: String = ""
    @Published var username = ""
    
    var isValid: AnyPublisher<Bool, Never> {
        Publishers
            .CombineLatest($username, $password)
            .receive(on: RunLoop.main)
            .map { username, password in
                return  username.count > 1 && password.count > 7
            }.eraseToAnyPublisher()
    }
    
    var inputValidUsername: Bool {
        validationMessageUsername == nil
    }
        
        
    init() {
        // you subscribe to any changes to the email field input
        $username
        // you ignore the first empty value that it gets initialised with
            .dropFirst()
        // you give the user a bit of time to finish typing
            .debounce(for: 0.6, scheduler: RunLoop.main)
        // you get rid of duplicated inputs as they do not change anything in terms of validation
            .removeDuplicates()
        // you validate the input string and in case of problems publish an error message
            .map { input in
                guard !input.isEmpty else {
                    return "Username cannot be left empty"
                }
                // in case the input is valid the error message is nil
                return nil
            }
        // you publish the error message for the view to react to
            .assign(to: &$validationMessageUsername)
    }
   
}
