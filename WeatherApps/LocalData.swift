//
//  LocalData.swift
//  WeatherApps
//
//  Created by R+IOS on 25/05/22.
//

import Foundation

class LocalData {
    func saveResponse(response: ResponseWeather) {
        do {
            // Create JSON Encoder
            let encoder = JSONEncoder()

            // Encode Note
            let data = try encoder.encode(response)

            // Write/Set Data
            UserDefaults.standard.set(data, forKey: "weather")

        } catch {
            print("Unable to Encode Array of News (\(error))")
        }
    }
    
    func getResponseLocal() -> ResponseWeather? {
        var response: ResponseWeather?
        
        if let data = UserDefaults.standard.data(forKey: "weather") {
            do {
                // Create JSON Decoder
                let decoder = JSONDecoder()

                // Decode Note
                response = try decoder.decode(ResponseWeather.self, from: data)

            } catch {
                print("Unable to Decode Notes (\(error))")
            }
        }
        
        return response ?? nil
    }
}
