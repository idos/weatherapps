//
//  Weather.swift
//  WeatherApps
//
//  Created by R+IOS on 24/05/22.
//

import Foundation

struct ResponseWeather: Codable {
    var lon: Double
    var lat: Double
    var timezone: String
    var daily: [Daily]
    var current: Current
}

struct Current: Codable {
    var pressure: Int
    var humidity: Int
    var wind_speed: Double
    var feels_like: Double
    var weather: [Weather]
}

struct Daily: Codable {
    var dt: Double
    var pressure: Int
    var humidity: Int
    var wind_speed: Double
    var weather: [Weather]
}

struct Weather: Codable {
    var id: Int
    var main: String
    var description: String
}

extension Double {
    func roundDouble() -> String {
        return String(format: "%.0f", self)
    }
    
    func toDate() -> String {
        let date = Date(timeIntervalSince1970: self)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE"
//        dateFormatter.locale = Locale(identifier: "IDN")
        dateFormatter.timeZone = .current
        let localDate = dateFormatter.string(from: date)
        
        return localDate
    }
    
    func convertToCelsius() -> String {
        return String(format: "%.0f", self - 273.15)
    }
}

