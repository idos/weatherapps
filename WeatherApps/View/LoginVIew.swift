//
//  LoginVIew.swift
//  WeatherApps
//
//  Created by R+IOS on 24/05/22.
//

import SwiftUI

struct LoginVIew: View {
    @State var username: String = ""
    @State var password: String = ""
    
    @StateObject var viewModel = LoginViewModel()
    @State var loginButtonDisabled = true
    @State var goMain = false
    
    var body: some View {
        ZStack {
            Color(.blue)
                .edgesIgnoringSafeArea(.all)
            
            VStack {
                WelcomeText()
                
                UserImage()
                
                TextField("Username", text: $viewModel.username)
                    .padding()
                    .background(lightGreyColor)
                    .cornerRadius(5.0)
                    .padding(.bottom, 20)
                    .foregroundColor(Color.black)
                
                SecureField("Password", text: $viewModel.password)
                    .padding()
                    .background(lightGreyColor)
                    .cornerRadius(5.0)
                    .padding(.bottom, 20)
                    .foregroundColor(Color.black)
                
                Button(action: {
                    self.goMain = true
                }) {
                    LoginButtonContent()
                        .background(self.loginButtonDisabled ? Color.gray : Color.green)
                }
                .disabled($loginButtonDisabled.wrappedValue)
                .onReceive(self.viewModel.isValid) { isValid in
                    self.loginButtonDisabled = !isValid
                }
                .background(self.loginButtonDisabled ? .gray : .green)
                
                
            }
            .padding()
            
            NavigationLink(isActive: self.$goMain) {
                MainView()
            } label: {
                EmptyView()
            }.hidden()
        }
    }
}

struct LoginVIew_Previews: PreviewProvider {
    static var previews: some View {
        LoginVIew()
    }
}

struct WelcomeText : View {
    var body: some View {
        return Text("Welcome!")
            .font(.largeTitle)
            .fontWeight(.semibold)
            .padding(.bottom, 20)
    }
}

struct UserImage : View {
    var body: some View {
        return Image("profile")
            .resizable()
            .aspectRatio(contentMode: .fill)
            .frame(width: 150, height: 150)
            .clipped()
            .cornerRadius(150)
            .padding(.bottom, 75)
    }
}

struct LoginButtonContent : View {
    var body: some View {
        return Text("LOGIN")
            .font(.headline)
            .foregroundColor(.white)
            .padding()
            .frame(width: 220, height: 60)
            .cornerRadius(15.0)
    }
}

let lightGreyColor = Color(red: 239.0/255.0, green: 243.0/255.0, blue: 244.0/255.0, opacity: 1.0)

