//
//  RowWheater.swift
//  WeatherApps
//
//  Created by R+IOS on 24/05/22.
//

import SwiftUI

struct RowWeather: View {
    var day: String = ""
    var id: Int = 0
    var main: String = ""
    var description: String = ""
    
    var body: some View {
        HStack(alignment: .center, spacing: 5){
            Text(day)
                .frame(width: 90, alignment: .leading)
            
            Text("\(id)")
                .frame(width: 50, alignment: .leading)
            
            Text(main)
                .frame(width: 70, alignment: .leading)
            
            Spacer()
            
            Text(description)
                .frame(alignment: .leading)
        }
        .padding([.leading, .trailing], -20)
    }
}

struct RowWeather_Previews: PreviewProvider {
    static var previews: some View {
        RowWeather()
    }
}
