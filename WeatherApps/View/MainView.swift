//
//  MainView.swift
//  WeatherApps
//
//  Created by R+IOS on 24/05/22.
//

import SwiftUI

struct MainView: View {
    init() {
        UITabBar.appearance().backgroundColor = UIColor.gray

    }
    
    var body: some View {
        ZStack {
            Color(.blue)
                .edgesIgnoringSafeArea(.all)
            
            TabView {
                HomeView()
                    .tabItem {
                        Label("Home", systemImage: "list.dash")
                    }
                
                ProfileView()
                    .tabItem {
                        Label("Profile", systemImage: "contacts")
                    }
            }
            .background(Color.white)
            .navigationTitle("")
            .navigationBarHidden(true)
            .navigationBarBackButtonHidden(true)
            
        }
    }
}

struct MainView_Previews: PreviewProvider {
    static var previews: some View {
        MainView()
    }
}
