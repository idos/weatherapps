//
//  HomeView.swift
//  WeatherApps
//
//  Created by R+IOS on 24/05/22.
//

import SwiftUI

struct HomeView: View {
    @StateObject var viewModel: WeatherViewModel = WeatherViewModel()
    
    var body: some View {
        ZStack() {
            ScrollView {
                VStack {
                    VStack(alignment: .leading, spacing: 5) {
                        Text("Weather")
                            .bold().font(.title)
                        Text("Today, \(Date().formatted(.dateTime.month().day().hour().minute()))")
                            .fontWeight(.light)
                        Text("Time Zone, \(self.viewModel.response?.timezone ?? "")")
                    }
                    .frame(maxWidth: .infinity, alignment: .leading)
                    
                    Spacer()
                    
                    VStack {
                        HStack {
                            VStack(spacing: 20) {
                                Image(systemName: "moon.fill")
                                    .font(.system(size: 40))
                                Text(self.viewModel.response?.current.weather[0].main ?? "")
                            }
                            .frame(width: 150, alignment: .leading)
                            
                            Spacer()
                            
                            Text("\(self.viewModel.response?.current.feels_like.convertToCelsius() ?? "0")  °")
                                .font(.system(size: 42))
                                .fontWeight(.bold)
                                .padding()
                        }
                        
                        HStack(spacing: 20) {
                            Image(systemName: "wind")
                                .font(.title2)
                                .frame(width: 20, height: 20)
                                .padding()
                                .background(Color(hue: 1.0, saturation: 0.0, brightness: 0.888))
                                .cornerRadius(50)
                            
                            Spacer()
                            
                            VStack(alignment: .trailing, spacing: 8) {
                                Text("Wind speed").font(.caption)
                                
                                Text("\(self.viewModel.response?.current.wind_speed.roundDouble() ?? "") m/s")
                                    .bold()
                                    .font(.title)
                            }
                        }
                        
                        HStack(spacing: 20) {
                            Image(systemName: "pressure")
                                .font(.title2)
                                .frame(width: 20, height: 20)
                                .padding()
                                .background(Color(hue: 1.0, saturation: 0.0, brightness: 0.888))
                                .cornerRadius(50)
                            
                            Spacer()
                            
                            VStack(alignment: .trailing, spacing: 8) {
                                Text("Pressure").font(.caption)
                                
                                Text("\(self.viewModel.response?.current.pressure ?? 0) hPa")
                                    .bold()
                                    .font(.title)
                            }
                        }
                        
                        HStack(spacing: 20) {
                            Image(systemName: "humidity")
                                .font(.title2)
                                .frame(width: 20, height: 20)
                                .padding()
                                .background(Color(hue: 1.0, saturation: 0.0, brightness: 0.888))
                                .cornerRadius(50)
                            
                            Spacer()
                            
                            VStack(alignment: .trailing, spacing: 8) {
                                Text("Humidity").font(.caption)
                                
                                Text("\(self.viewModel.response?.current.humidity ?? 0)  %")
                                    .bold()
                                    .font(.title)
                            }
                        }
                        
                        Spacer()
                            .frame(height: 30)
                    }
                    .frame(maxWidth: .infinity)
                    
                    Spacer()
                    
                    if viewModel.response != nil {
                        List(viewModel.response!.daily, id: \.dt) { item in
                            
                            RowWeather(day: item.dt.toDate(), id: item.weather[0].id, main: item.weather[0].main, description: item.weather[0].description)
                        }
                        .frame(maxWidth: .infinity, alignment: .leading)
                        .padding([.leading, .trailing], -20)
                        .frame(height: CGFloat(viewModel.response?.daily.count ?? 0) * CGFloat(50))
                    }
                    
                    Spacer()
                }
                .padding()
                .frame(maxWidth: .infinity, alignment: .leading)
                .navigationTitle("")
                .navigationBarHidden(true)
                .navigationBarBackButtonHidden(true)
            }
        }
        .background(Color(hue: 0.644, saturation: 0.799, brightness: 0.393))
        .preferredColorScheme(.dark)
        .onAppear {
            self.viewModel.getData()
        }
    }
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
    }
}
